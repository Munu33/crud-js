const mongoose = require('mongoose')


const userSchema = new mongoose.Schema({
 
  name:{
    type:String,
    required:true
  },
  age:{

     type:Number,
     required:true
  },
  martialstatus:{
     type:String,
     required:true
 },
  dob: {
  type: Date
 },
  mobileNumber: {
  type: String,
   required: true
 },
 state:{
  type:String,
  required:true
},
city:{
  type:String,
  required:true
},
isDeleted:{
  type:Boolean
}



 
});

module.exports = mongoose.model('User',userSchema)