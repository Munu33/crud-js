const express = require('express')
const mongoose = require('mongoose')
const url = 'mongodb://localhost/userDB'

const app = express()

mongoose.connect(url, {useNewUrlParser:true})
const con = mongoose.connection

con.on('open', () => {
    console.log('connected...')
})

app.use(express.json())

const userRouter = require('./routes/users.route')
app.use('/users',userRouter)

app.listen(8000, () => {
    console.log('Server started')
})