const express = require('express')
const router = express.Router()
const userController = require('../controllers/user.contoller');

router.route("/").post(userController.createUser).get(userController.getUsers);

router
  .route("/:userId")
  .get(userController.getUser)
  .put(userController.updateUser)
  .delete(userController.deleteUser);



module.exports = router